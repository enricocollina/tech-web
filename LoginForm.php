<?php if(!isset($_SESSION)){session_start();}ob_start(); ?>
<!DOCTYPE html>
<html lang="it">
    <head>
        <?php require 'template/inoutHead.php'; ?>
        <link rel="stylesheet" href="css/loginForm.css">
    </head>

<body>
    <div class="container">
        <div class="d-flex justify-content-center h-100">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <a href="index.php"><i class="fas text-center col-sm-2 fa-2x fa-home text-white icon-allign"></i></a>
                        <h3 class="text-center col-sm-9">Accedi al tuo account</h3>
                    </div>
                </div>
                <div class="card-body mx-5">
                    <form name="login" method="POST" action="Login.php">
                        <div class="input-group form-group">
                            <input type="email" class="form-control" placeholder="username(email)" name="username">
                        </div>
                        <div class="input-group form-group">
                            <input type="password" class="form-control" placeholder="password" name="password" id="password">
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Login" class="btn login_btn" onclick="formhash(this.form, this.form.password);" />
                        </div>
                    </form>
                    <?php
                        if(isset($_SESSION["error"])){
                            if($_SESSION["error"] == 1){
                                echo "<script type=\"text/javascript\">wrongPassword();</script>";
                            } else if ($_SESSION["error"] == 2){
                                echo "<script type=\"text/javascript\">userNotFound();</script>";
                            } else if ($_SESSION["error"] == 3){
                                echo "<script type=\"text/javascript\">missingData();</script>";
                            }
                            unset($_SESSION["error"]);
                        }
                    ?>
                </div>
                <div class="card-footer">
                    <div class="d-flex justify-content-center links">
                        Non hai un account?<a href="SubscribeForm.php">registrati ora</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
<?php ob_end_flush(); ?>