function formhash(form, password, password2) {
    // Crea un elemento di input che verrà usato come campo di output per la password criptata.
    let p = document.createElement("input");
    let p2 = document.createElement("input");
    // Aggiungi un nuovo elemento al tuo form.
    form.appendChild(p);
    form.appendChild(p2);
    p.name = "p";
    p.type = "hidden";
    p.value = hex_sha512(password.value);
    p2.name = "p2";
    p2.type = "hidden";
    p2.value = hex_sha512(password2.value);
    console.log(p2);
    // Assicurati che la password non venga inviata in chiaro.
    password.value = "";
    password2.value = "";
    // Come ultimo passaggio, esegui il 'submit' del form.
    form.submit();
 }