$(document).ready(function () {
})

function loginSuccessful(){
    toastr.success("Login effettuato!");
}

function registrationSuccessful(){
    toastr.success("Registrazione effettuata!");
}

function userNotFound(){
    toastr.error("Utente non esistente!");
}

function userAlreadyRegistered(){
    toastr.error("L'email è già registrata!");
}

function wrongPassword(){
    toastr.error("Password errata!");
}

function differentPassword(){
    toastr.error("Le password inserite sono diverse!");
}

function missingData(){
    toastr.error("Dati non inseriti!");
}

function registrationAborted(){
    toastr.error("Registrazione fallita!");
}