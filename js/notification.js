function read(i){
    let cls = i.getAttribute("class");
    let id = i.getAttribute("id");
    let type = id.includes("bookmarkOrg") ? 1 : 0; //1=organizzatore, 0=utente
    id = id.split("-")[1];
    let url = "ReadNotification.php";
    let read;
    if(cls.includes("fas")){
        read = 0;
        i.setAttribute("class", "far fa-2x fa-bookmark text-danger");
    } else {
        read = 1;
        i.setAttribute("class", "fas fa-2x fa-bookmark text-danger");
    }
    let done = $.post(url, {typeOfRead: type, idRow: id, read: read, wait: 1});
    done.done(function(data){
        toastr.success("Modifica salvata");
    }).fail(function(jqXHR, textStatus, error){
        toastr.error(error);
    });
}