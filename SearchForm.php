<?php if(!isset($_SESSION)){session_start();}ob_start(); ?>
<!DOCTYPE html>
<html lang="it">
<head>
        <?php require 'template/head.php'; ?>
        <link rel="stylesheet" href="css/search.css">
        <script src="js/searchForm.js"></script>
</head>

<body>
    <?php require 'template/nav.php'; ?>
    <div class="container col-12 mt-3">
        <div class="row">
            <div class="col-lg-5 mb-3">
                <div class="row">
                    <div class="col-12">
                        <div class="px-3 bg-filters">
                            <h2 class="text-center pt-2"><b>Filtri</b></h2>
                            <form class="px-3" onsubmit="return checkDate()">
                                <div class="form-group">
                                    <h3>Categorie</h3>

                                    <div class="pl-4">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="sport">
                                            <label class="custom-control-label text-unselectable"
                                                for="sport">Sport</label>
                                        </div>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="cinema">
                                            <label class="custom-control-label text-unselectable"
                                                for="cinema">Cinema</label>
                                        </div>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="teatro">
                                            <label class="custom-control-label text-unselectable"
                                                for="teatro">Teatro</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h3>Prezzo</label>
                                        <input type="range" id="rangeFilter" class="custom-range pl-4" min="10"
                                            max="100" id="prezzo">
                                </div>
                                <div class="form-group">
                                    <h3>Località</h3>
                                    <div class="pl-4">
                                        <input type="text" class="form-control">
                                    </div>

                                </div>

                                <div class="form-group">
                                    <h3>Data evento</h3>
                                    <div class="pl-4">
                                        <label for="dataDal">Dal</label>
                                        <input id="dataDal" onchange="checkDate()" class="form-control" type="date">

                                        <label for="dataAl">Al</label>
                                        <input id="dataAl" onchange="checkDate()" class="form-control" type="date">
                                    </div>
                                </div>

                                <div class="text-right pb-4">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="row">
                <?php   if(isset($templateParams["risultati"])){
                            foreach ($templateParams["risultati"] as $risultato):
                        ?>
                    <div class="col-sm-4">
                        <img class="p-2 img-size" src=<?php echo IMG_DIR.$risultato["urlFoto"]?>>
                    </div>
                    <div class="col-sm-8">
                        <h2><a href="Detail.php?id=<?php echo $risultato["id"]?>"><?php echo $risultato["nome"]?></a></h2>
                        <p><?php echo $risultato["descrizione"]?></p>
                    </div>
                <?php
                        endforeach; 
                    }
                ?>
                </div>
                <hr class="hr text-center w-90">
            </div>
        </div>
    </div>
    </div>
</body>

</html>