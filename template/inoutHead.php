
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="fontawesome/css/all.min.css">
    <link rel="stylesheet" href="toastr/toastr.min.css">
    <link rel="stylesheet" href="css/nav.css">
    <script src="js/sha512.js"></script>
    <script src="js/form.js"></script>
    <script src="JQuery/jquery-3.4.1.js"></script>
    <script src="toastr/toastr.min.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="js/login&registrationToastr.js"></script>
    <title>e-buy</title>
