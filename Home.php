<?php if(!isset($_SESSION)){session_start();}ob_start();?>
<!DOCTYPE html>
<html lang="it">
    <head>
        <?php require 'template/head.php'; ?>
        <link rel="stylesheet" href="css/home.css">
    </head>



<body>
    <?php require 'template/nav.php'; ?>
    <div class="container col-sm-10">
        <div class="row">
        <?php foreach($templateParams["eventi"] as $evento): ?>      
            <div class="col-sm-6 col-md-4 col-lg-3 mb-3">
                <div class="card">
                    <a href="Detail.php?id=<?php echo $evento["id"]; ?>">
                        <div class="card">
                            <img class="card-img-top" src="<?php echo IMG_DIR.$evento["urlFoto"]; ?>" alt="">
                            <div class="card-body">
                                <h5 class="card-title"><?php echo $evento["nome"] ?></h5>
                                <p class="card-text"><?php echo $evento["descrizione"]?></p>
                                <p class="font-weight-bolder">Prezzo: <?php echo $evento["prezzo"]?> $</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        <?php endforeach; ?>
        </div>
    </div>
    <?php
        if(isset($_SESSION["return"])){
            echo $_SESSION["return"];
            unset($_SESSION["return"]);
        }
        if(isJustLogged()){
            echo "<script type=\"text/javascript\">loginSuccessful();</script>";
            logged();
        } else if (isJustRegistered()){
            echo "<script type=\"text/javascript\">registrationSuccessful();</script>";
            registered();
        }
    ?>
</body>
</html>