<?php
class DatabaseHelper{
    private $db;

    public function __construct($servername, $username, $password, $dbname){
        $this->db = new mysqli($servername, $username, $password, $dbname);
        $this->db->set_charset("utf8");
        if($this->db->connect_error){
            die("Connection failed: " + $db->connect_error);
        }
    }
    
//Public function
    public function addToCart($userData){
        $query = "SELECT COUNT(*) AS righe, quantità FROM dettaglicarrello WHERE idCarrello=? AND idEvento=?;";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii', $userData["idCarrello"], $userData["idEvento"]);
        $stmt->execute();
        $result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        if($result[0]["righe"] == 0){
            $query = "INSERT INTO  dettaglicarrello (id, idCarrello, idEvento, quantità) VALUES (NULL, ?, ?, ?)";
            $stmt->prepare($query);
            $stmt->bind_param('iii', $userData["idCarrello"], $userData["idEvento"], $userData["numeroBiglietti"]);
        } else if ($result[0]["righe"] > 0){
            if($result[0]["quantità"] + $userData["numeroBiglietti"] > 6){
                $query = "UPDATE dettaglicarrello SET quantità = 6 WHERE idCarrello = ? AND idEvento = ?";
                $stmt->prepare($query);
                $stmt->bind_param('ii', $userData["idCarrello"], $userData["idEvento"]);
            } else {
                $query = "UPDATE dettaglicarrello SET quantità = quantità + ? WHERE idCarrello = ? AND idEvento = ?";
                $stmt->prepare($query);
                $stmt->bind_param('iii', $userData["numeroBiglietti"], $userData["idCarrello"], $userData["idEvento"]);
            }
        }
        $stmt->execute();
        $result = $stmt->get_result();
        if($this->db->errno != 0){
            return false;
        }
        return true;
    }

    public function checkEmail($email){
        $stmt = $this->db->prepare("SELECT email FROM utenti WHERE email=?");
        $stmt->bind_param('s', $email);
        $stmt->execute();
        $result = $stmt->get_result();
        $result->fetch_all(MYSQLI_ASSOC);
        if($result->num_rows > 0){
            return false;
        }
        return true;
    }

    public function deleteFromCart($userData){
        $query = "DELETE FROM dettaglicarrello WHERE idCarrello = ? AND idEvento = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii', $userData["idCarrello"], $userData["idEvento"]);
        $stmt->execute();
        $result = $stmt->get_result();
        if($this->db->errno != 0){
            return false;
        }
        return true;
    }

    public function generalSearchEvent($str){
        $query = "SELECT * FROM eventi WHERE eventi.nome LIKE CONCAT('%',?,'%') OR eventi.descrizione LIKE CONCAT('%',?,'%')";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ss', $str, $str);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getAllEvents(){
        $query = "SELECT eventi.*, utenti.email FROM eventi JOIN utenti ON eventi.idOrganizzatore=utenti.id";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getCart($cartId){
        $query = "  SELECT eventi.id AS idEvento, nome, descrizione, urlFoto, prezzo, quantità FROM dettaglicarrello
                    JOIN eventi ON dettaglicarrello.idEvento = eventi.id WHERE dettaglicarrello.idCarrello = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $cartId);
        $stmt->execute();
        $result = $stmt->get_result();
        $stmt->close();
        $cart = $result->fetch_all(MYSQLI_ASSOC);

        return $cart;
    }

    public function getCategories(){
        $stmt = $this->db->prepare("SELECT * FROM categorie");
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getCategoryById($idcategory){
        $stmt = $this->db->prepare("SELECT nome FROM categorie WHERE id=?");
        $stmt->bind_param('i',$idcategory);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEventById($idEvent){
        $query = "SELECT id, nome, idCategoria, descrizione, indirizzo, luogo, città, DATE_FORMAT(data, \"%d %M %Y %H:%i\") as data, numeroBiglietti, numeroBigliettiVenduti, prezzo, urlFoto FROM eventi WHERE id=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$idEvent);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEvents($idUser){
        $query = "SELECT * FROM eventi WHERE eventi.idOrganizzatore = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $idUser);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getManagerNotifications($idUser){
        $query = "SELECT notificheorganizzatore.*, eventi.nome, eventi.urlFoto FROM notificheorganizzatore JOIN eventi ON notificheorganizzatore.idEvento = eventi.id
                    WHERE notificheorganizzatore.idUtente = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $idUser);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getUserNotifications($idUser){
        $query = "SELECT notificheutente.*, eventi.nome, eventi.urlFoto 
        FROM notificheutente JOIN ordini ON notificheutente.idOrdine = ordini.id
        JOIN eventi ON eventi.id = notificheutente.idEvento
        WHERE ordini.idUtente = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $idUser);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getOrders($idUser){
        $query = "  SELECT ordini.proprietarioCarta, ordini.data, eventi.id, eventi.nome, eventi.prezzo, eventi.urlFoto, dettagliordine.quantità 
                    FROM ordini JOIN dettagliordine ON ordini.id=dettagliordine.idOrdine JOIN eventi ON eventi.id=dettagliordine.idEvento
                    WHERE ordini.idUtente=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $idUser);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getRandomEvents($n = 50){
        $stmt = $this->db->prepare("SELECT id, nome, descrizione, prezzo, urlFoto FROM eventi ORDER BY RAND() LIMIT ?");
        $stmt->bind_param('i',$n);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function insertOrder($userdata){
        $id = uniqid (rand (),true);
        $query = "INSERT into ordini (id, idUtente, proprietarioCarta, numeroCarta, cvv, data) VALUES (?, ?, ?, ?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sisiis', $id, $userdata["id"], $userdata["proprietario"], $userdata["carta"], $userdata["cvv"], $userdata["data"]);
        $stmt->execute();
        $result = $stmt->get_result();
        if($this->db->errno != 0){
            return false;
        }

        $stmt->close();

        $cartId = $userdata["idCarrello"];

        $cart = $this->getCart($cartId);    

        $query = "INSERT INTO dettagliordine (id, idOrdine, idEvento, quantità) VALUES(NULL, ?, ?, ?)";
        $stmt = $this->db->prepare($query);
        foreach($cart as $c):
                $stmt->bind_param('sii', $id, $c["idEvento"], $c["quantità"]);
                $stmt->execute();
                $result = $stmt->get_result();
                if($this->db->errno != 0){
                    return false;
                }
        endforeach;
        
        if(!$this->deleteCart($cartId)){
            return false;
        }
        return true;
    }

    public function insertUpdateEvent($event){
        
        $eventId = $event["idEvento"];
        
        if(isset($eventId) && $eventId != 0){ // sono in modifica
            
            $query = "  UPDATE eventi SET nome = ?, 
                        idCategoria = ?,
                        descrizione = ?, 
                        indirizzo = ?, 
                        luogo = ?, 
                        città = ?, 
                        eventi.data = ?, 
                        numeroBiglietti = ?, 
                        prezzo = ?,
                        urlFoto = ?  
                        WHERE id = ?";

            $stmt = $this->db->prepare($query);

            $stmt->bind_param(  'sisssssiisi', 
                                $event["nome"], 
                                $event["idCategoria"],
                                $event["descrizione"], 
                                $event["indirizzo"], 
                                $event["luogo"], 
                                $event["città"], 
                                $event["data"], 
                                $event["numeroBiglietti"], 
                                $event["prezzo"],
                                $event["urlFoto"],
                                $eventId
                            );
            $stmt->execute();
            $result = $stmt->get_result();
            if($this->db->errno != 0){
                return false;
            }
            $stmt->close();

            $query = "SELECT * FROM dettagliordine WHERE idEvento = ?";
            $stmt = $this->db->prepare($query);
            $stmt->bind_param('i', $eventId);
            $stmt->execute();
            $orders = $stmt->get_result();
            if($this->db->errno != 0){
                return false;
            }
            $stmt->close();
            $query = "INSERT INTO notificheutente (id, idEvento, idOrdine, descrizione, letto) VALUES (NULL, ?, ?, \"ATTENZIONE! L'evento è stato modificato!\", 0)";
            $stmt = $this->db->prepare($query);
            foreach($orders as $order):
                $stmt->bind_param('is', $eventId, $order["idOrdine"]);
                $stmt->execute();
                $result = $stmt->get_result();
                if($this->db->errno != 0){
                    return false;
                }
            endforeach;
            return true;
        }else{

            $query = "  INSERT INTO eventi (id, 
            idOrganizzatore, 
            nome, 
            idCategoria,
            descrizione, 
            indirizzo, 
            luogo, 
            città, 
            eventi.data, 
            numeroBiglietti, 
            prezzo, 
            urlFoto)  
            VALUES(NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

            $stmt = $this->db->prepare($query);

            $stmt->bind_param(  'isisssssiis', 
                                $event["idOrganizzatore"],
                                $event["nome"],
                                $event["idCategoria"], 
                                $event["descrizione"], 
                                $event["indirizzo"], 
                                $event["luogo"], 
                                $event["città"], 
                                $event["data"], 
                                $event["numeroBiglietti"], 
                                $event["prezzo"],
                                $event["urlFoto"]
                            );

            $stmt->execute();
            $result = $stmt->get_result();
            if($this->db->errno != 0){
                return false;
            }
            return true;
        }
    }

    public function login($user){
        $query = "  SELECT utenti.id as iduser, email as username, nome, seed, password, organizzatore, amministratore, carrelli.id as idcarrello FROM utenti JOIN carrelli
                    ON utenti.id = carrelli.idUtente WHERE email = ? LIMIT 1";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $user);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function readNotification($notification){
        if($notification["tabella"] == "notificheorganizzatore"){
            $query = "UPDATE notificheorganizzatore SET LETTO = ? WHERE id = ?";
        } else {
            $query = "UPDATE notificheutente SET LETTO = ? WHERE id = ?";
        }
        if($notification["letta"] == 0){
            $letta = 0;
        } else {
            $letta = 1;
        }
        $riga = $notification["riga"];
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("ii", $letta, $riga);
        $stmt->execute();
        $result = $stmt->get_result();
        if($this->db->errno != 0){
            return false;
        }
        return true;
    }

    public function register($user){
        $query = "INSERT into utenti (id, nome, cognome, email, password, seed, dataNascita, organizzatore, amministratore) VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, '0')";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ssssssi', $user["nome"], $user["cognome"], $user["email"], $user["password"], $user["seed"], $user["dataNascita"], $user["organizzatore"]);
        $stmt->execute();
        $result = $stmt->get_result();
        if($this->db->errno != 0){
            return false;
        }
        $query2 = "INSERT INTO carrelli (id, idUtente) VALUES(NULL, (SELECT id FROM utenti where email = ?))";
        $stmt = $this->db->prepare($query2);
        $stmt->bind_param('s', $user["email"]);
        $stmt->execute();
        $result = $stmt->get_result();
        if($this->db->errno != 0){
            return false;
        }
        return true;
    }

    public function removeFromCart($userData){
        $query = "UPDATE dettaglicarrello SET quantità = ? WHERE idCarrello = ? AND idEvento = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('iii', $userData["quantità"], $userData["idCarrello"], $userData["idEvento"]);
        $stmt->execute();
        $result = $stmt->get_result();
        if($this->db->errno != 0){
            return false;
        }
        return true;
    }

    public function updateSoldTickets(){
        $query = "SELECT dettagliordine.idEvento, SUM(dettagliordine.quantità) AS totale FROM dettagliordine GROUP BY dettagliordine.idEvento";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $events = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        $stmt->close();

        $query = "UPDATE eventi SET numeroBigliettiVenduti = ? WHERE id = ?";
        $stmt = $this->db->prepare($query);
        foreach ($events as $event):
            $stmt->bind_param('ii', $event["totale"], $event["idEvento"]);
                $stmt->execute();
                $result = $stmt->get_result();
                if($this->db->errno != 0){
                    return false;
                }
        endforeach;
        return true;
    }

//Private function
    private function deleteCart($cartId){
        $query = "DELETE FROM dettaglicarrello WHERE idCarrello = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $cartId);
        $stmt->execute();
        $result = $stmt->get_result();
        if($this->db->errno != 0){
            return false;
        }
        return true;
    }
}
?>