<?php
require_once 'bootstrap.php';
if(!isUserLoggedIn()){
    header("HTTP/1.0 500 User non loggato");
    exit;
} else {
    if(!isset($_POST["idCarrello"]) || !isset($_POST["idEvento"]) || !isset($_POST["quantità"])){
        header("HTTP/1.0 500 Parametri non validi");
        exit;
    } else {
        $userData["idCarrello"] = $_POST["idCarrello"];
        $userData["idEvento"] = $_POST["idEvento"];
        $userData["quantità"] = $_POST["quantità"];
        if($dbh->removeFromCart($userData)){
            header("HTTP/1.0 200 Ok");
            exit;
        } else {
            header("HTTP/1.0 500 Errore durante la modifica");
            exit;
        }
    }
}
?>