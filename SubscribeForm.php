<?php if(!isset($_SESSION)){session_start();}ob_start(); ?>
<!DOCTYPE html>
<html lang="it">
    <head>
        <?php require 'template/inoutHead.php';?>
        <link rel="stylesheet" href="css/subscribeForm.css">
    </head>

<body>
    <div class="container">
        <div class="d-flex justify-content-center h-100">
            <div class="card col-sm-6">
                <div class="card-header">
                    <div class="row">
                        <a href="index.php"><i class="fas text-center col-sm-2 fa-2x fa-home text-white icon-allign"></i></a>
                        <h3 class="text-center col-sm-9">Iscriviti</h3>
                    </div>
                </div>
                <div class="card-body mx-5">
                    <form name="subscribe" method="POST" action="Subscribe.php">
                        <div class="row">
                            <div class="input-group p-0 pr-sm-2 col-sm-6 form-group">
                                <input type="text" class="form-control" placeholder="nome" name="nome">
                            </div>
                            <div class="input-group p-0 col-sm-6 form-group">
                                <input type="text" class="form-control" placeholder="cognome" name="cognome">
                            </div>
                            <div class="input-group form-group">
                                <input type="email" class="form-control" placeholder="email" name="email">
                            </div>
                            <div class="input-group form-group">
                                <input type="password" class="form-control" placeholder="password" name="password" id="password">
                            </div>
                            <div class="input-group form-group">
                                <input type="password" class="form-control" placeholder="conferma password" name="password2" id="password2">
                            </div>
                            <div class="input-group form-group">
                                <input type="date" class="form-control" placeholder="data" name="data">
                            </div>
                            <div class="custom-control text-white input-group custom-checkbox form-group">
                                <input type="checkbox" class="custom-control-input" id="organizzatore" name="organizzatore">
                                <label class="custom-control-label text-unselectable"
                                for="organizzatore">Organizzatore</label>
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Registrati" class="btn login_btn" onclick="formhash(this.form, this.form.password, this.form.password2);">
                            </div>
                        </div>
                    </form>
                    <?php
                        if(isset($_SESSION["error"])){
                            if($_SESSION["error"] == 1){
                                echo "<script type=\"text/javascript\">registrationAborted();</script>";
                            } else if ($_SESSION["error"] == 2){
                                echo "<script type=\"text/javascript\">userAlreadyRegistered();</script>";
                            } else if ($_SESSION["error"] == 3){
                                echo "<script type=\"text/javascript\">differentPassword();</script>";
                            } else if ($_SESSION["error"] == 4){
                                echo "<script type=\"text/javascript\">missingData();</script>";
                            }
                            unset($_SESSION["error"]);
                        }
                    ?>
                </div>
                <div class="card-footer">
                    <div class="d-flex justify-content-center links">
                        Hai già un account?<a href="LoginForm.php">fai il login</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>