<?php if(!isset($_SESSION)){session_start();}ob_start();?>
<!DOCTYPE html>
<html lang="it">
<head>
<?php require 'template/head.php'; ?>
<script src="js/checkout.js"></script>
<link rel="stylesheet" href="css/checkoutForm.css">
</head>

<body>
    <div class="container">
        <div class="d-flex justify-content-center h-100">
            <div class="card col-sm-10">
                <div class="py-4 text-light text-center">
                    <div class="row">
                        <div class="col-2">
                        <a href="index.php"><i
                                class="fas text-center fa-2x fa-home text-white"></i></a>
                        </div>
                        <h3 class="text-center col-8">Checkout</h3>
                    </div>
                </div>
                <div class="row">
                    <table align = "center" class="table table-dark w-90">
                        <thead>
                            <tr>
                                <th scope="col" class="w-100">Nome</th>
                                <th scope="col" class="w-0">Quantità</th>
                                <th scope="col" class="w-0">Prezzo</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $tot=0; foreach($templateParams["biglietti"] as $biglietto): ?>
                            <tr>
                                <td class="text-truncate mw-1"><?php echo $biglietto["nome"] ?></td>
                                <td class="no-wrap"><?php echo $biglietto["quantità"] ?></td>
                                <td class="no-wrap"><?php echo ($biglietto["prezzo"] * $biglietto["quantità"]); $tot += ($biglietto["prezzo"] * $biglietto["quantità"])?>€</td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <div class="card-body mx-3">
                    <form name="buyTicket-<?php echo getCartId() ?>" id="buyTicket" action="BuyTicket.php">
                        <div class="row">
                            <div class="input-group form-group">
                                <input type="text" class="form-control" placeholder="Proprietario carta" name="proprietario">
                            </div>
                            <div class="input-group p-0 pr-sm-2 col-sm-6  form-group">
                                <input type="text" class="form-control" placeholder="Numero carta" name="numeroCarta">
                            </div>
                            <div class="input-group p-0 col-sm-6  form-group">
                                <input type="text" class="form-control" placeholder="CVV" name="cvv">
                            </div>
                            <h3 class="input-group text-light"><strong>Totale: </strong> <?php echo $tot?>€</h3>

                            <div class="form-group">
                                <input type="submit" value="Acquista" class="btn checkout_btn">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>

</html>