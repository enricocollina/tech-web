<?php
require_once 'bootstrap.php';
if(!isUserLoggedIn()){
    require 'LoginForm.php';
} else if(strlen($_POST["titolo"]) == 0 || strlen($_POST["categoria"]) == 0 || strlen($_POST["descrizione"]) == 0
            || strlen($_POST["indirizzo"]) == 0 || strlen($_POST["luogo"]) == 0 || strlen($_POST["citta"]) == 0
            || strlen($_POST["dataEvento"]) == 0 || strlen($_POST["biglietti"]) == 0 || strlen($_POST["prezzo"]) == 0){
            $_SESSION["return"] = "<script type=\"text/javascript\">toastr.error(\"Parametri non validi\")</script>";
        require 'Events.php';
        exit;
} else {
    
    $target_file = getcwd()."\img\\".basename($_FILES["image"]["name"]);

    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    // Check if image file is a actual image or fake image
    if(isset($_POST["submit"])) {
        $check = getimagesize($_FILES["image"]["name"]);
        if($check === false) {
            $_SESSION["return"] = "<script type=\"text/javascript\">toastr.error(\"Il file non è un'immagine\")</script>";
            require 'index.php';
            exit;
        }
    }
    // Check if file already exists
    if (file_exists($target_file)) {
        $_SESSION["return"] = "<script type=\"text/javascript\">toastr.error(\"Non è stato possibile caricare l'immagine, esiste già con lo stesso nome\")</script>";
        require 'index.php';
        exit;
    }
    // Check file size
    if ($_FILES["image"]["size"] > 5000000) {
        $_SESSION["return"] = "<script type=\"text/javascript\">toastr.error(\"Non è stato possibile caricare l'immagine per la sua dimensione\")</script>";
        require 'index.php';
        exit;
    }
    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
        $_SESSION["return"] = "<script type=\"text/javascript\">toastr.error(\"Non è stato possibile caricare l'immagine perchè non è nei formati supportati\")</script>";
        require 'index.php';
        exit;
    }
 
    if (!move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
        $_SESSION["return"] = "<script type=\"text/javascript\">toastr.error(\"Non è stato possibile caricare l'immagine\")</script>";
        require 'index.php';
        exit;
    }
}
    $event["idEvento"] = $_POST["id"];
    $event["idOrganizzatore"] = getUserId();
    $event["nome"] = $_POST["titolo"];
    $event["idCategoria"] = $_POST["categoria"];
    $event["descrizione"] = $_POST["descrizione"];
    $event["indirizzo"] = $_POST["indirizzo"];
    $event["luogo"] = $_POST["luogo"];
    $event["città"] = $_POST["citta"];
    $event["data"] = $_POST["dataEvento"];
    $event["numeroBiglietti"] = $_POST["biglietti"];
    $event["prezzo"] = $_POST["prezzo"];
    $event["urlFoto"] = basename($_FILES["image"]["name"]);
    if($dbh->insertUpdateEvent($event)){
        $_SESSION["return"] = "<script type=\"text/javascript\">toastr.success(\"Evento inserito correttamente\")</script>";
        require 'index.php';
        header("location:./index.php");
        exit;
    }

?>